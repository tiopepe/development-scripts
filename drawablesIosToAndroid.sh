#!/bin/bash

##############################################################################
# This script is takes the iphone drawables and places them into 
# android drawable-mdpi and drawable-xhdpi in an android friendly naming way.
#
# Must be run where the png files are. Does not support recursive directory 
# listing
#
##############################################################################


#we create the directories mdpi and xhdpi
mkdir drawable-mdpi
mkdir drawable-xhdpi

#copy the @2x files to xhdpi
ls -l | grep @2x.png | awk {'print $9'} | xargs -n1 -t -I {} cp {} drawable-xhdpi/

#copy the not-@2x files to mdpi
ls -l | grep .png | grep -v @2x | awk {'print $9'} | xargs -n1 -t -I {} cp {} drawable-mdpi/



#enter mdpi
cd drawable-mdpi

#capitalize each file
ls | awk '{system("mv " $0 " " toupper(substr($0,1,1)) substr($0,2))}'

#rename each file for its android name
for f in *.png
do
 #echo "renaming $f to `echo $f | sed -e 's/\([A-Z]\)/\_\1/g' -e 's/\_//' | tr '[:upper:]' '[:lower:]'`"
 mv $f `echo $f | sed -e 's/\([A-Z]\)/\_\1/g' -e 's/\_//' | tr '[:upper:]' '[:lower:]'`
done




#enter xhdpi
cd ../drawable-xhdpi

for f in *@2x.png
do
	name=${f:(-`echo ${#f}`):(`echo $((${#f}-7))`)}.png
	#echo "renaming $f to $name"
	mv $f $name
done

#capitalize each file
ls | awk '{system("mv " $0 " " toupper(substr($0,1,1)) substr($0,2))}'

#rename each file for its android name
for f in *.png
do
 #echo "renaming $f to `echo $f | sed -e 's/\([A-Z]\)/\_\1/g' -e 's/\_//' | tr '[:upper:]' '[:lower:]'`"
 mv $f `echo $f | sed -e 's/\([A-Z]\)/\_\1/g' -e 's/\_//' | tr '[:upper:]' '[:lower:]'`
done

