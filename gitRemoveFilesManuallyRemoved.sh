#!/bin/bash

##############################################################################
#
# This script marks manually deleted files for removal in git
#
##############################################################################

git status | grep deleted | awk {'print $3'} | xargs git rm 